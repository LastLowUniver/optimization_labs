#!flask/bin/python
from flask import Flask, jsonify, request, render_template
from mainModel import MainModel
from annealingMetod import AnnealingMetod

app = Flask(__name__, template_folder="client")

model = MainModel()  # lab 6
modelSeven = AnnealingMetod()  # lab 7

# route for working at lab 6
@app.route('/', methods=['GET'])  
def home_route():
    return render_template('index.html')

# route for working at lab 7
@app.route('/lab', methods=['GET'])
def lab_route():
    return render_template('lab.html')  

# route for getting datd lab 6
@app.route('/decision', methods=['GET'])
def getDecisionLabSix():
    x1, x2 = model.useMethod(request.args.get('method'), int(request.args.get('indexfunc')), [
                             float(request.args.get('x1')), float(request.args.get('x2'))], float(request.args.get('eps')))
    return jsonify({'result': [x1, x2]})


# route for getting datd lab 7
@app.route('/decisiontwo', methods=['GET']) 
def getDecisionLabSeven():
    try:
        x1, x2 = modelSeven.useMethod(request.args.get('method'), [float(request.args.get('x1')), float(
            request.args.get('x2'))], 1, int(request.args.get('indexfunc')), int(request.args.get('n')))
    # При использовании коши, бывают появляются ошибки (через раз), из-за использования в нем random, поэтому добавлен отлавливатель, которой перезапустит получение
    except Exception:
        x1, x2 = modelSeven.useMethod(request.args.get('method'), [float(request.args.get('x1')), float(
            request.args.get('x2'))], 1, int(request.args.get('indexfunc')), int(request.args.get('n')))

    return jsonify({'result': [x1, x2]})


if __name__ == '__main__':
    app.run(debug=True)
