import numpy as np
from typing import Callable, List
import math

from best_samples import best_samples
from adaptive_method import adaptive_method


class MainModel(object):
    functions = [
        lambda x: 4 * (x[0] - 5) ** 2 + (x[1] - 6) ** 2,
        #  пропуск добл функции
        lambda x: -20 * math.exp(-0.2 * math.sqrt(0.5 * (x[0] ** 2 + x[1] ** 2))) - math.exp(
            0.5 * (math.cos(2 * 3.14 * x[0]) + math.cos(2 * 3.14 * x[1]))) + 2.71828 + 20,
        lambda x: (1.5 - x[0] + x[0] * x[1]) ** 2 + (2.25 - x[0] + x[0]
                                                     * x[1] ** 2) ** 2 + (2.625 - x[0] + x[0] * x[1] ** 3) ** 2,
        lambda x: 100 * \
        math.sqrt(abs(x[1] - 0.01 * x[0] ** 2)) + 0.01 * abs(x[0] + 10),
        lambda x: 0.26 * (x[0] ** 2 + x[1] ** 2) - 0.48 * x[0] * x[1],
        lambda x: 100 * (x[1] - x[0] ** 2) ** 2 + (x[0] - 1) ** 2
    ]

    methods = {"adaptive": adaptive_method, "best": best_samples}

    def useMethod(this, nameMethod, indexFunc, x0, eps: float = 0.0001):
        print(str(this.functions[indexFunc]))
        return this.methods[nameMethod](
            this.functions[indexFunc],
            x0,
            eps
        )
