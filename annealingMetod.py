import math

from quick_cauchy import QA
from boltzmann_method import boltzmann_method


class AnnealingMetod(object):

    def f1(x):
        x, y = x
        return 0.5 + (math.cos(math.sin(math.fabs(x**2 - y**2)))**2 - 0.5) / (1 + 0.001*(x**2 + y**2))**2


    def f2(x):
        x, y = x
        return -math.fabs(math.sin(x) * math.cos(y) * math.exp(math.fabs(1 - (x**2 + y**2)**0.5/math.pi)))


    def f3(x):
        x, y = x
        return -math.cos(x) * math.cos(y) * math.exp(-((x - math.pi)**2 + (y - math.pi)**2))


    functions = [ f1, f2, f3 ]

    def getFunctions(_, x0, choice_yp):
        if (choice_yp == 1):
            x, y = x0
            # Функция Шаффера N4
            return 0.5 + (math.cos(math.sin(math.fabs(x**2 - y**2)))**2 - 0.5) / (1 + 0.001*(x**2 + y**2))**2

        if (choice_yp == 2):
            x, y = x0
            # Табличная функция Хольдера
            return -math.fabs(math.sin(x) * math.cos(y) * math.exp(math.fabs(1 - (x**2 + y**2)**0.5/math.pi)))

        if (choice_yp == 3):
            x, y = x0
            # Функция Изома
            return -math.cos(x) * math.cos(y) * math.exp(-((x - math.pi)**2 + (y - math.pi)**2))

    methods = {"qa": QA, "boltzmann": boltzmann_method}

    def useMethod(this, nameMethod, x0, t0, indexFunc, N=10000):
        return this.methods[nameMethod](
            x0,
            t0,
            this.functions[indexFunc],
            indexFunc,
            N
        )
