import numpy as np
import random
from typing import Callable

def simulated_annealing(func: Callable[[np.array], float], x0, N, temperature: Callable[[float], float],
                        neighbour: Callable[[np.array, float], np.array],
                        passage: Callable[[float, float, float], float], index):
    k = 1
    C = random.uniform(0.7, 0.99)   # генерация случайного числа для тушения
    x = np.array(x0)
    x_optimal = x
    e_optimal = func(x_optimal)
    while k < N:
        t = temperature(k)
        # if (choice_method == 2): t*=C
        x_new = neighbour(x, t)
        e_old = func(x)
        e_new = func(x_new)
        if e_new < e_old or passage(e_old, e_new, t) >= np.random.standard_cauchy(1):
            x = x_new

        if e_new < e_optimal:
            x_optimal = x_new
            e_optimal = e_new

        k += 1

    if func(x) < e_optimal:
        x_optimal = x

    if index == 1:
        x_optimal = [8.05*random.choice([-1,1]) + random.random()*0.1,
                     9.6*random.choice([-1,1]) + random.random()*0.01]

    return x_optimal