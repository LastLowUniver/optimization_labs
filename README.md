### Приложение для решения методами оптимизации

```
Приложение, написанное на Python, при использовании flask
```

#### Установка
```
pip install numpy typing scipy flask
```

#### Запуск
``` 
python3 mainController.py
```
[Код методов взят с https://github.com/avdosev/optimization_methods](https://github.com/avdosev/optimization_methods)

###### Проект для Волгоградского технического университета
